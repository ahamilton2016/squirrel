//
//  VBStyle_Input_Picker.swift
//  Squirrel
//
//  Created by Alexander Hamilton on 1/28/18.
//  Copyright © 2018 Vibrant, LLC. All rights reserved.
//

import UIKit

extension VBInput_Picker {
    
    static func styling( view: VBInput_Picker ) {
        
        view.frame = CGRect( x: 0, y: 0, width: VBviewwidth, height: 60 )
        
        view.button.frame = CGRect( x: 15, y: 0, width: Double(view.frame.size.width)-30, height: 60 )
        view.button.backgroundColor = .white
        view.button.setTitleColor(.black, for: .normal)
        view.button.layer.borderColor = UIColor.darkGray.cgColor
        view.button.layer.borderWidth = 0.3
        view.button.layer.cornerRadius = 5
        view.button.layer.masksToBounds = true
        
    }
    
}
