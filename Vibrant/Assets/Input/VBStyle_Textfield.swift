//
//  VBStyle_Textfield.swift
//  MilkTheMoment
//
//  Created by Alexander Hamilton on 1/12/18.
//  Copyright © 2018 Milk The Moment. All rights reserved.
//

import UIKit

extension VBTextfield {
    
    static func styling( view: VBTextfield ) {

        view.frame = CGRect( x: 0, y: 0, width: VBviewwidth, height: 60 )
        
        view.tf.frame = CGRect( x: 15, y: 0, width: Double(view.frame.size.width)-30, height: 60 )
        view.tf.backgroundColor = .white
        view.tf.layer.borderColor = UIColor.darkGray.cgColor
        view.tf.layer.borderWidth = 0.3
        view.tf.layer.cornerRadius = 5
        view.tf.layer.masksToBounds = true
        
        let paddingView = UIView(frame: CGRect( x: 0, y: 0, width: 15, height: view.tf.frame.height))
        view.tf.leftView = paddingView
        view.tf.leftViewMode = UITextFieldViewMode.always
        
    }
    
}
