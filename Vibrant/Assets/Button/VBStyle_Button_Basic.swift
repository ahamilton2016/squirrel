//
//  VBStyle_Button_Basic.swift
//  MilkTheMoment
//
//  Created by Alexander Hamilton on 1/13/18.
//  Copyright © 2018 Milk The Moment. All rights reserved.
//

import UIKit

extension VBButton_Basic {
    
    static func styling( view: VBButton_Basic ) {
        
        view.frame = CGRect( x: 0, y: 0, width: VBviewwidth, height: 60 )
        
        view.button.frame = CGRect( x: 15, y: 0, width: Double(view.frame.size.width)-30, height: 60 )
        if (VBBrand.get.color("primary")) != nil {
            view.button.backgroundColor = VBBrand.get.color("primary")
        } else {
            view.button.backgroundColor = UIColor.blue
        }
//        view.button.layer.borderColor = UIColor.darkGray.cgColor
//        view.button.layer.borderWidth = 0.3
        view.button.layer.cornerRadius = 5
        view.button.layer.masksToBounds = true
        
    }
    
}
