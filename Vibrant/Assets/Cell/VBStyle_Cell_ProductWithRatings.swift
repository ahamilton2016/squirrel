//
//  VBStyle_Cell_ProductWithRatings.swift
//  MilkTheMoment
//
//  Created by Alexander Hamilton on 1/21/18.
//  Copyright © 2018 Milk The Moment. All rights reserved.
//

import UIKit

extension VBCell_ProductWithRatings {
    
    static func styling( view: VBCell_ProductWithRatings ) {
        
        view.titlelabel.frame = CGRect( x: 10, y: 10, width: 250, height: 20 )
        view.titlelabel.font = UIFont.systemFont(ofSize: 17)
        view.titlelabel.textColor = UIColor.darkGray
        
        let ivsize = CGFloat(60)
        view.iv.frame = CGRect( x: CGFloat(VBviewwidth)-ivsize-10, y: view.titlelabel.frame.origin.y, width: ivsize, height: ivsize )
        view.iv.contentMode = .scaleAspectFill
        view.iv.clipsToBounds = true
        
        view.ratingsview.frame = CGRect( x: 10, y: Double(view.titlelabel.frame.origin.y+view.titlelabel.frame.size.height), width: Double(view.titlelabel.frame.size.width), height: 15 )
        for i in 0 ... 4 {
            let graystar = UIImageView()
            let goldstar = UIImageView()
            var originx = CGFloat(0)
            if i > 0 {
                let laststar = view.graystarsarray[i-1]
                originx = laststar.frame.origin.x+laststar.frame.size.width+2
            }
            graystar.frame = CGRect( x: originx, y: 0, width: view.ratingsview.frame.size.height, height: view.ratingsview.frame.size.height )
            graystar.image = UIImage(named: "star_gray.png")
            graystar.contentMode = .scaleAspectFit
            view.ratingsview.addSubview(graystar)
            
            view.graystarsarray.append(graystar)
            
            goldstar.frame = CGRect( x: originx, y: 0, width: view.ratingsview.frame.size.height, height: view.ratingsview.frame.size.height )
            goldstar.image = UIImage(named: "star_gold.png")
            goldstar.contentMode = .scaleAspectFit
            view.ratingsview.addSubview(goldstar)
            goldstar.isHidden = true
            
            view.goldstarsarray.append(goldstar)
        }
        
        
        view.descriptionlabel.frame = CGRect( x: 10, y: Double(view.ratingsview.frame.origin.y+view.ratingsview.frame.size.height)+5, width: Double(view.titlelabel.frame.size.width), height: 20 )
        view.descriptionlabel.font = UIFont.systemFont(ofSize: 14)
        view.descriptionlabel.textColor = UIColor.darkGray
        
        
        view.separator.backgroundColor = UIColor.init(red: (223/255.0), green: (223/255.0), blue: (223/255.0), alpha: 1)
        
    }
    
}
