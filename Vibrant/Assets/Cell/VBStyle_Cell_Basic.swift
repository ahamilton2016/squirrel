//
//  VBStyle_Cell_Basic.swift
//  MilkTheMoment
//
//  Created by Alexander Hamilton on 1/15/18.
//  Copyright © 2018 Milk The Moment. All rights reserved.
//

import UIKit

extension VBCell_Basic {
    
    static func styling( view: VBCell_Basic ) {
        
        view.titlelabel.frame = CGRect( x: 10, y: 10, width: VBviewwidth - 20, height: 20 )
        view.titlelabel.font = UIFont.boldSystemFont(ofSize: 18)
        view.titlelabel.textColor = UIColor.darkGray
        
        view.descriptionlabel.frame = CGRect( x: 10, y: Double(view.titlelabel.frame.origin.y+view.titlelabel.frame.size.height)+10, width: Double(view.titlelabel.frame.size.width), height: 20 )
        view.descriptionlabel.font = UIFont.systemFont(ofSize: 14)
        view.descriptionlabel.textColor = UIColor.darkGray
        
    }
    
}
