//
//  VBStyle_Cell_ImageText_Full.swift
//  MilkTheMoment
//
//  Created by Alexander Hamilton on 1/14/18.
//  Copyright © 2018 Milk The Moment. All rights reserved.
//

import UIKit

extension VBCell_ImageText_Full {
    
    static func styling( view: VBCell_ImageText_Full ) {
        
//        view.frame = CGRect( x: 0, y: 0, width: VBviewwidth, height: 200 )
        
        view.containerview.frame = CGRect( x: 10, y: 10, width: VBviewwidth-20, height: 330 )
        view.containerview.layer.borderColor = UIColor.darkGray.cgColor
        view.containerview.layer.borderWidth = 0.2
        view.containerview.layer.masksToBounds = true
        view.containerview.layer.cornerRadius = 2
        
        view.featuredimage.frame = CGRect( x: 0, y: 0, width: view.containerview.frame.size.width, height: 250 )
        view.featuredimage.contentMode = .scaleAspectFill
        view.featuredimage.clipsToBounds = true
        view.clipsToBounds = true
        
        view.titlelabel.frame = CGRect( x: 10, y: Double(view.featuredimage.frame.origin.y+view.featuredimage.frame.size.height)+10, width: 200, height: 20 )
        view.titlelabel.font = UIFont.boldSystemFont(ofSize: 16)
        view.titlelabel.textColor = UIColor.darkGray
        
        view.descriptionlabel.frame = CGRect( x: Double(view.titlelabel.frame.origin.x), y: Double(view.titlelabel.frame.origin.y+view.titlelabel.frame.size.height)+5, width: 270, height: 30 )
        view.descriptionlabel.numberOfLines = 0
        view.descriptionlabel.text = "idk testing"
        
        let buttoncolor = UIColor.init(red: (143/255.0), green: (143/255.0), blue: (147/255.0), alpha: 1)
        let actionbuttonheight = Double(30)
        let actionbuttonwidth = Double(80)
        let containerheight = Double(view.containerview.frame.size.height)
        let featuredimagebottomy = Double( view.featuredimage.frame.origin.y+view.featuredimage.frame.size.height )
        view.actionbutton.frame = CGRect( x: Double(view.containerview.frame.size.width)-actionbuttonwidth-10, y: (containerheight-((containerheight-featuredimagebottomy)/2.0))-(actionbuttonheight/2.0), width: actionbuttonwidth, height: actionbuttonheight )
        view.actionbutton.layer.cornerRadius = 5
        view.actionbutton.layer.masksToBounds = true
        view.actionbutton.setTitle("Learn More", for: .normal)
        view.actionbutton.isUserInteractionEnabled = false
        view.actionbutton.setTitleColor(buttoncolor, for: .normal)
        view.actionbutton.backgroundColor = .clear
        view.actionbutton.layer.borderWidth = 1.1
        view.actionbutton.layer.borderColor = buttoncolor.cgColor
        view.actionbutton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 12)
        
    }
    
}
