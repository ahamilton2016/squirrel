//
//  VBStyle_Section_TickerView.swift
//  MilkTheMoment
//
//  Created by Alexander Hamilton on 1/21/18.
//  Copyright © 2018 Milk The Moment. All rights reserved.
//

import UIKit

extension VBSection_TickerView {
    
    static func styling( view: VBSection_TickerView ) {
        
        view.frame = CGRect( x: 0, y: 0, width: VBviewwidth, height: 350 )
        
        view.titlelabel.frame = CGRect( x: 20, y: 0, width: view.frame.size.width, height: 26 )
        view.titlelabel.textColor = .white
        view.titlelabel.font = UIFont.systemFont(ofSize: 23, weight: UIFont.Weight.bold)
        
        
        view.containerview.frame = CGRect( x: view.titlelabel.frame.origin.x, y: view.titlelabel.frame.origin.y+view.titlelabel.frame.size.height+5, width: view.frame.size.width-40, height: (view.frame.size.height) )
        
        
    }
    
}
