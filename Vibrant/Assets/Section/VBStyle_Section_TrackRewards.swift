//
//  VBStyle_Section_TrackRewards.swift
//  MilkTheMoment
//
//  Created by Alexander Hamilton on 1/14/18.
//  Copyright © 2018 Milk The Moment. All rights reserved.
//

import UIKit

extension VBSection_TrackRewards {
    
    static func styling( view: VBSection_TrackRewards ) {
        
        view.frame = CGRect( x: 0, y: 0, width: VBviewwidth, height: 100 )
        
        view.titlelabel.frame = CGRect( x: 15, y: 15, width: 150, height: 15 )
        view.pointvalue.frame = CGRect( x: 15, y: Double(view.titlelabel.frame.origin.y+view.titlelabel.frame.size.height+5), width: 70, height: 50 )
        view.pointlabel.frame = CGRect( x: Double(view.pointvalue.frame.origin.x + view.pointvalue.frame.size.width), y: Double(view.pointvalue.frame.origin.y+view.pointvalue.frame.size.height-30), width: Double(view.frame.size.width)-30, height: 30 )
        view.messagelabel.frame = CGRect( x: Double(view.frame.size.width - 250 - 15), y: Double(view.titlelabel.frame.origin.y+16), width: 250, height: 20 )
        view.barview.frame = CGRect( x: Double(view.messagelabel.frame.origin.x), y: Double(view.messagelabel.frame.origin.y+view.messagelabel.frame.size.height)+8, width: Double(view.messagelabel.frame.size.width), height: 15 )
        view.fillview.frame = CGRect( x: 1, y: 1, width: 0, height: Double(view.barview.frame.size.height-2) )
        
        view.barview.layer.cornerRadius = view.barview.frame.size.height/2.0
        view.barview.layer.masksToBounds = true
        view.fillview.layer.cornerRadius = view.fillview.frame.size.height/2.0
        view.fillview.layer.masksToBounds = true
        
        view.titlelabel.font = UIFont.systemFont(ofSize: 10)
        view.pointvalue.font = UIFont.boldSystemFont(ofSize: 35)
        view.pointlabel.font = UIFont.systemFont(ofSize: 10)
        view.pointvalue.textAlignment = .right
        view.messagelabel.font = UIFont.systemFont(ofSize: 15)
        view.barview.backgroundColor = UIColor.init(white: 0, alpha: 0.3)
        
        view.messagelabel.textAlignment = .right
        
    }
    
}
