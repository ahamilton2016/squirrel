//
//  VBStyle_Global.swift
//  MilkTheMoment
//
//  Created by Alexander Hamilton on 1/12/18.
//  Copyright © 2018 Milk The Moment. All rights reserved.
//

import Foundation
import UIKit

var VBviewwidth = Double(0)
var VBviewheight = Double(0)
var VBcurrentvc = UIViewController()

extension NSObject {
    
    static func stringClassFromString(_ className: String) -> AnyClass! {
        
        /// get namespace
        let namespace = Bundle.main.infoDictionary!["CFBundleExecutable"] as! String;
        
        /// get 'anyClass' with classname and namespace
        let cls: AnyClass = NSClassFromString("\(namespace).\(className)")!;
        
        // return AnyClass!
        return cls;
    }
    
}



extension UIViewController {
    
    func setVC(_ vc: UIViewController ) {
        
        VBcurrentvc = vc
        VBviewwidth = Double(self.view.frame.size.width)
        VBviewheight = Double(self.view.frame.size.height)
        
    }
}
