//
//  VBSection.swift
//  MilkTheMoment
//
//  Created by Alexander Hamilton on 1/14/18.
//  Copyright © 2018 Milk The Moment. All rights reserved.
//

import UIKit

class VBSection: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    static func set( superview: UIView, filename: String, identifier: String ) {
        VBCollection.addViewToQueue(superview: superview, viewtype: "VBSection", filename: filename, identifier: identifier )
    }
    
    public static func viewsetup( superview: UIView, vdict: [String: Any] ) -> UIView {
        let filename = vdict["filename"] as? String ?? ""
        var view: UIView?
        
        if filename == "VBSection_TrackRewards" {
            let newview = VBSection_TrackRewards()
            newview.viewdict = VBData.getValue(source: vdict["identifier"]!) as! [[String: Any]]
            newview.viewoptions = VBData.getOptions(identifier: vdict["identifier"] as! String)
            newview.identifier = vdict["identifier"] as! String
            newview.viewsetup( superview: vdict["superview"] as! UIView )
            view = newview
        } else if filename == "VBSection_ImageText_Full" {
            let newview = VBSection_ImageText_Full()
            newview.viewdict = VBData.getValue(source: vdict["identifier"]!) as! [String: Any]
            newview.viewoptions = VBData.getOptions(identifier: vdict["identifier"] as! String)
            newview.identifier = vdict["identifier"] as! String
            newview.viewsetup( superview: vdict["superview"] as! UIView )
            view = newview
        } else if filename == "VBSection_TickerView" {
            let newview = VBSection_TickerView()
            newview.viewdict = VBData.getValue(source: vdict["identifier"]!) as! [String: Any]
            newview.viewoptions = VBData.getOptions(identifier: vdict["identifier"] as! String)
            newview.identifier = vdict["identifier"] as! String
            newview.viewsetup( superview: vdict["superview"] as! UIView )
            view = newview
        }
        
        return view!
    }
    
}
