//
//  VBViews.swift
//  MilkTheMoment
//
//  Created by Alexander Hamilton on 1/13/18.
//  Copyright © 2018 Milk The Moment. All rights reserved.
//

import UIKit

class VBViews: NSObject {
    
    public static func viewsetup( superview: UIView, viewtype: String, vdict: [String: Any] ) -> UIView {
    
        var view: UIView?
        if viewtype == "VBInput" {
            view = VBInput.viewsetup( superview: superview, vdict: vdict )
        } else if viewtype == "VBButton" {
            view = VBButton.viewsetup( superview: superview, vdict: vdict )
        } else if viewtype == "VBSection" {
            view = VBSection.viewsetup( superview: superview, vdict: vdict )
        } else if viewtype == "VBTable" {
            view = VBTable.viewsetup( superview: superview, vdict: vdict )
        }
        
        return view!
    }

}
