//
//  VBNavigate.swift
//  MilkTheMoment
//
//  Created by Alexander Hamilton on 1/13/18.
//  Copyright © 2018 Milk The Moment. All rights reserved.
//

import UIKit

class VBNavigate: NSObject {

    public static func push(vc: String, animated: Bool = true) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let tovc = storyboard.instantiateViewController(withIdentifier: vc)
        VBcurrentvc.navigationController?.pushViewController(tovc, animated: animated)
    }
    
    public static func present(vc: String, animated: Bool = true) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let tovc = storyboard.instantiateViewController(withIdentifier: vc)
        VBcurrentvc.present(tovc, animated: animated, completion: nil)
    }
    
    public static func presentToNav(vc: String, animated: Bool = true) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let tovc = storyboard.instantiateViewController(withIdentifier: vc)
        let navc = UINavigationController(rootViewController: tovc)
        VBcurrentvc.present(navc, animated: animated, completion: nil)
    }
    
}
