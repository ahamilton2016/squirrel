//
//  VBCollection.swift
//  MilkTheMoment
//
//  Created by Alexander Hamilton on 1/12/18.
//  Copyright © 2018 Milk The Moment. All rights reserved.
//

import UIKit

class VBCollection: NSObject {
    static var queueCollection = [[String: Any]]()
    static var viewCollection = [String: UIView]()
    
    static func addViewToQueue( superview: UIView, viewtype: String, filename: String, identifier: String ) {
        
//        if queueCollection[identifier] == nil {
//            print("WARNING: the view '" + identifier + "' is not being tracked because it already exists!")
//            return
//        } else {
//            queueCollection[identifier] = view
        self.queueCollection.append(["viewtype": viewtype, "filename": filename, "identifier": identifier, "superview": superview])
//            return true
//        }
        
    }
    
    static func placeViews() {
        var i=0
        for v in self.queueCollection {
            var lastdict = [String: Any]()
            var lastview = UIView()
            var lastsuperview = UIView()
            if i>0 {
                lastdict = self.queueCollection[i-1]
                lastview = self.viewCollection[lastdict["identifier"] as! String]!
                lastsuperview = lastdict["superview"] as! UIView
            }
            let superview = v["superview"] as! UIView
            let view = VBViews.viewsetup( superview: superview, viewtype: v["viewtype"] as! String, vdict: v )
            if i > 0 {
                if lastsuperview == superview {
                    VBStyling.placeUnderView( view: view, underview: lastview )
                }
            }
            self.viewCollection[v["identifier"] as! String] = view
            
            i += 1
        }
    }
    
    public static func getView( identifier: String ) -> UIView {
        return viewCollection[identifier]!
    }
    public static func getPageViewsInOrder() -> [UIView] {
        var views = [UIView]()
        for q in queueCollection {
            views.append(viewCollection[q["identifier"] as! String]!)
        }
        return views
    }
    
    public static func getTextfieldView( identifier: String ) -> VBTextfield {
        return getView( identifier: identifier ) as! VBTextfield
    }
    
    public static func getButtonView( identifier: String ) -> VBButton_Basic {
        return getView( identifier: identifier ) as! VBButton_Basic
    }
    
    public static func clearArrays() {
        queueCollection.removeAll()
        viewCollection.removeAll()
        VBData.clearArrays()
    }
    
    
    
}
