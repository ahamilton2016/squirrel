//
//  VBInput.swift
//  MilkTheMoment
//
//  Created by Alexander Hamilton on 1/12/18.
//  Copyright © 2018 Milk The Moment. All rights reserved.
//

import UIKit

class VBInput: UIView {
//    static var vbcollection = VBCollection()
    
    
    public static func set( superview: UIView, filename: String, identifier: String ) {
        VBCollection.addViewToQueue(superview: superview, viewtype: "VBInput", filename: filename, identifier: identifier )
    }
    
    public static func viewsetup( superview: UIView, vdict: [String: Any] ) -> UIView {
        let filename = vdict["filename"] as? String ?? ""
        var view: UIView?
        
        if filename == "VBTextfield" {
            let newview = VBTextfield()
            newview.viewoptions = VBData.getOptions(identifier: vdict["identifier"] as! String)
            newview.viewsetup( superview: vdict["superview"] as! UIView )
            view = newview
        } else if filename == "VBInput_Picker" {
            let newview = VBInput_Picker()
            newview.viewdict = VBData.getData(identifier: vdict["identifier"] as! String) as? [String : Any] ?? [:]
            newview.viewoptions = VBData.getOptions(identifier: vdict["identifier"] as! String)
            newview.viewsetup( superview: vdict["superview"] as! UIView )
            view = newview
        }
        
        return view!
    }
    
}
