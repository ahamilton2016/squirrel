//
//  VBStorage.swift
//  MilkTheMoment
//
//  Created by Alexander Hamilton on 1/13/18.
//  Copyright © 2018 Milk The Moment. All rights reserved.
//

import UIKit

class VBStorage: NSObject {
    
    public static func store(key: String, value: Any) {
        let defaults = UserDefaults.standard
        if getStored(key: key) != "" {
            defaults.removeObject(forKey:key)
        }
        defaults.set(value, forKey: key)
    }
    
    public static func getStored(key: String) -> String {
        let defaults = UserDefaults.standard
        let returningvalue = defaults.string(forKey: key)
        
        return returningvalue ?? ""
    }
    
    public static func storeData(key: String, data: Data) {
        let filename = VBStorage.getDocumentsDirectory().appendingPathComponent(key)
        try? data.write(to: filename)
    }
    public static func getStoredData(key: String) -> UIImage? {
        let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
        let nsUserDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
        let paths               = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
        if let dirPath          = paths.first
        {
            let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent(key)
            let image    = UIImage(contentsOfFile: imageURL.path)
            return image
        } else {
            return nil
        }
    }
    
    public static func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    public static func clearAllStored() {
        UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
        UserDefaults.standard.synchronize()
    }
    
}
