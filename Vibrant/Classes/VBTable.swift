//
//  VBTable.swift
//  MilkTheMoment
//
//  Created by Alexander Hamilton on 1/14/18.
//  Copyright © 2018 Milk The Moment. All rights reserved.
//

import UIKit

class VBTable: UIView {
    
    var viewdict = [[String: Any]]()
    var viewoptions = [String: Any]()
    var identifier = String()

    static func set( superview: UIView, filename: String, identifier: String ) {
        VBCollection.addViewToQueue(superview: superview, viewtype: "VBTable", filename: filename, identifier: identifier )
    }
    
    public static func viewsetup( superview: UIView, vdict: [String: Any] ) -> UIView {
        
        let filename = vdict["filename"] as? String ?? ""
        var view: UIView?
        
        if filename == "VBTable_Default" {
            let newview = VBTable_Default()
            newview.viewdict = VBData.getValue(source: vdict["identifier"]!) as! [[String: Any]]
            newview.viewoptions = VBData.getOptions(identifier: vdict["identifier"] as! String)
            newview.identifier = vdict["identifier"] as! String
            newview.viewsetup( superview: vdict["superview"] as! UIView )
            view = newview
        }
        
        return view!
    }
    
    public static func setCell( name: String, identifier: String ) {
        
        
        
    }

}
