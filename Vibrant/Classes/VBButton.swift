//
//  VBButton.swift
//  MilkTheMoment
//
//  Created by Alexander Hamilton on 1/13/18.
//  Copyright © 2018 Milk The Moment. All rights reserved.
//

import UIKit

class VBButton: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }

    static func set( superview: UIView, filename: String, identifier: String ) {
        VBCollection.addViewToQueue(superview: superview, viewtype: "VBButton", filename: filename, identifier: identifier )
    }
    
    public static func viewsetup( superview: UIView, vdict: [String: Any] ) -> UIView {
        let filename = vdict["filename"] as? String ?? ""
        var view: UIView?
        
        if filename == "VBButton_Basic" {
            let newview = VBButton_Basic()
            newview.viewsetup( superview: vdict["superview"] as! UIView )
            view = newview
        }
        
        return view!
    }

}
