//
//  VBCell.swift
//  MilkTheMoment
//
//  Created by Alexander Hamilton on 1/14/18.
//  Copyright © 2018 Milk The Moment. All rights reserved.
//

import UIKit

class VBCell: UITableViewCell {

    
    
//    static func set( superview: UIView, filename: String, identifier: String ) {
//        VBCollection.addViewToQueue(superview: superview, viewtype: "VBCell", filename: filename, identifier: identifier )
//    }
    
//    public static func viewsetup( vdict: [String: Any] ) {
//        let filename = vdict["filename"] as? String ?? ""
//        var view: UIView?
//        
//        if filename == "VBButton_Basic" {
//            let newview = VBButton_Basic()
//            newview.viewsetup( superview: vdict["superview"] as! UIView )
//            view = newview
//        }
//        
//    }
    
    public static func getCellHeight( identifier: String ) -> Double {
        if identifier == "VBCell_ImageText_Full" {
            return 350
        } else if identifier == "VBCell_Basic" {
            return 50
        } else if identifier == "VBCell_ProductWithRatings" {
            return 80
        } else {
            return 200
        }
    }
    
}
