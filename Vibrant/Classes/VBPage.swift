//
//  VBPage.swift
//  MilkTheMoment
//
//  Created by Alexander Hamilton on 1/12/18.
//  Copyright © 2018 Milk The Moment. All rights reserved.
//

import UIKit

class VBPage: NSObject {
    
    static func start() {
        VBCollection.clearArrays()
    }
    
    static func end() {
        VBCollection.placeViews()
    }

}
