//
//  VBStyling.swift
//  MilkTheMoment
//
//  Created by Alexander Hamilton on 1/13/18.
//  Copyright © 2018 Milk The Moment. All rights reserved.
//

import UIKit

class VBStyling: NSObject {
    
    public static func adjustImageSize( image: UIImage, desiredwidth: Double? = 0, desiredheight: Double? = 0 ) -> CGSize {
        let oldwidth = Double(image.size.width)
        let oldheight = Double(image.size.height)
        var scalefactor = Double(0)
        if desiredwidth! > 0 {
            scalefactor = desiredwidth! / oldwidth
        } else {
            scalefactor = desiredheight! / oldheight
        }
        let newwidth = oldwidth * scalefactor
        let newheight = oldheight * scalefactor
        
        return CGSize( width: newwidth, height: newheight )
    }
    
    public static func drawGradient(_ view: UIView, startcolor: UIColor, endcolor: UIColor, from: String? = "", to: String? = "" ) {
        let layer = CAGradientLayer()
        layer.frame = view.frame
        layer.colors = [startcolor.cgColor, endcolor.cgColor]
        if from != "" && to != "" {
            
        }
        view.layer.addSublayer(layer)
    }
    
    public static func placeUnderView( view: UIView, underview: UIView ) {
        
        if view is VBTable {
            let tableview = view as! VBTable_Default
            tableview.frame = CGRect( x: tableview.frame.origin.x, y: underview.frame.origin.y+underview.frame.size.height+10, width: tableview.frame.size.width, height: tableview.frame.size.height - abs(tableview.frame.origin.y-(underview.frame.origin.y+underview.frame.size.height+10)) )
            tableview.thistable.frame = CGRect( origin: tableview.thistable.frame.origin, size: CGSize( width: tableview.thistable.frame.size.width, height: tableview.frame.size.height ) )
        } else {
            view.frame = CGRect( x: view.frame.origin.x, y: underview.frame.origin.y+underview.frame.size.height+10, width: view.frame.size.width, height: view.frame.size.height )
        }
        
        
    }
    
    public static func addMarginTop(_ view: UIView, margin: Double ) {
        view.frame = CGRect( x: view.frame.origin.x, y: view.frame.origin.y+CGFloat(margin), width: view.frame.size.width, height: view.frame.size.height )
    }
    
    public static func repositionViews() {
        let views = VBCollection.getPageViewsInOrder()
        var i=0
        for v in views {
            if i > 0 {
                let lastview = views[i-1]
                VBStyling.placeUnderView(view: v, underview: lastview)
            }
            i += 1
        }
        
    }
    
    public static func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }

}


