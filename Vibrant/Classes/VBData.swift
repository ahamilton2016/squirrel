//
//  VBData.swift
//  MilkTheMoment
//
//  Created by Alexander Hamilton on 1/15/18.
//  Copyright © 2018 Milk The Moment. All rights reserved.
//

import UIKit

class VBData: NSObject {
    
    static var viewDataCollection = [String: Any]()
    static var viewOptionsCollection = [String: Any]()
    
    public static func getValue( source: Any, key: String = "" ) -> Any? {
        
        if source is String {
            return VBData.getData(identifier: source as! String)
        } else if key != "" {
            var dictionary = source as? [String: Any] ?? [:]
            
            var value = dictionary[key] ?? ""
            if checkFormat( data: dictionary ) {
                let dict = dictionary["value"] as! [String: Any]
                value = dict[key] ?? ""
            }
            
            return value
        } else {
            return source 
        }
        
    }
    
    public static func clearArrays() {
        viewDataCollection.removeAll()
        viewOptionsCollection.removeAll()
    }
    
    public static func addData( data: Any, identifier: String ) {
        
        if VBData.checkFormat(data: data) {
            var dict = data as! [String: Any]
            var dbInfo = dict["db_info"] as! [String: Any]
            dbInfo["identifier"] = identifier
            dict["db_info"] = dbInfo
            let data = dict
            viewDataCollection[identifier] = data
        } else {
            viewDataCollection[identifier] = data
        }
        
    }
    
    public static func addOption( key: String, value: Any, identifier: String ) {
        
        var dict = [String: Any]()
        if viewOptionsCollection[identifier] != nil {
            dict = viewOptionsCollection[identifier] as! [String : Any]
            dict[key] = value
        } else {
            dict[key] = value
        }
        viewOptionsCollection[identifier] = dict
        
    }
    
    public static func getData( identifier: String ) -> Any? {
        return viewDataCollection[identifier] ?? []
    }
    
    public static func getOptions( identifier: String ) -> [String: Any] {
        return viewOptionsCollection[identifier] as? [String : Any] ?? [:]
    }
    
    public static func toValueAndDBInfo( data: Any, conditions: [[String: String]], tablename: String, customcolname: String="" ) -> Any {
        var tablenames = [String: Any]()
        var colnames = [String: Any]()
        var datakeys = [String]()
        if data is [String: Any] {
            let dict = data as! [String: Any]
            datakeys = Array(dict.keys)
        } else if data is [[String: Any]] {
            let array = data as! [[String: Any]]
            let dict = array[0]
            datakeys = Array(dict.keys)
        } else {
            return data
        }
        if datakeys.count > 0 {
            for k in datakeys {
                tablenames[k] = tablename
                if customcolname != "" {
                    colnames[k] = customcolname
                } else {
                    colnames[k] = k
                }
            }
        }
        let returnthisdict = [
            "value": data,
            "db_info": [
                "tablenames": tablenames,
                "colnames": colnames,
                "conditions": conditions,
                "identifier": ""
            ]
        ]
        return returnthisdict
    }
    
    public static func getIdentifier( data: Any ) -> String {
        if VBData.checkFormat(data: data) {
            let dict = data as! [String: Any]
            let dbInfo = dict["db_info"] as! [String: Any]
            return dbInfo["identifier"] as! String
        } else {
            return ""
        }
    }
    
    public static func checkFormat( data: Any ) -> Bool {
        
        if data is [String: Any] {
            let dict = data as! [String: Any]
            if dict["value"] != nil && dict["db_info"] != nil {
                return true
            }
        }
        
        return false
        
    }
    
    public static func convertKeys( dict: [String: Any], identifier: String ) -> [String: Any] {
        
        let viewoptions = VBData.getOptions(identifier: identifier)
        
        var mutabledict = dict
        if checkFormat(data: dict) {
            mutabledict = (dict["value"] as? [String: Any])!
        }
        // check if key is in convert keys option
        let convertibleKeys = viewoptions["convert_keys"] as? [String: String] ?? [:]
//        print("GOT CONVERTIBLE KEYS")
        for k in convertibleKeys {
            let newkey = k.value
            let newval = dict[k.key] ?? ""
            if newval is String {
                if newval as! String != "" {
                    mutabledict[newkey] = newval
                }
            } else {
                mutabledict[newkey] = newval
            }
        }
        
        var returningdict = dict
        if checkFormat(data: dict) {
            returningdict["value"] = mutabledict
        } else {
            returningdict = mutabledict
        }
//        print(returningdict)
        return returningdict
        
    }

}
