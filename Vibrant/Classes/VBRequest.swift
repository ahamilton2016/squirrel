//
//  VBRequest.swift
//  MilkTheMoment
//
//  Created by Alexander Hamilton on 1/13/18.
//  Copyright © 2018 Milk The Moment. All rights reserved.
//

import UIKit

extension NSMutableData {
    func appendString(string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: true)
        append(data!)
    }
}

class VBRequest: NSObject {

    public static func defaultcallback( json: [Any] ) {
        print(json)
    }
    public static func postRequest( urlstring: String, params: [[String: Any]], callback: @escaping ( [Any] ) -> Void = VBRequest.defaultcallback ) {
        
        let session = URLSession.shared
        
        let boundary = generateBoundaryString()
        
        let url = URL( string: urlstring )
        var request = URLRequest(url: url! )
        request.httpMethod = "POST"
        
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
//        print("PARAMS")
//        print(params)
        
        request.httpBody = createBodyWithParameters( parameters: params, boundary: boundary ) as Data
        
        //        request.httpBody.appendString(string: "--\(boundary)--\r\n")
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
//            dump(data!)
            //            if let json = (try? JSONSerialization.jsonObject(with: data!, options: .allowFragments) as! [String: AnyObject]) {
            //                print( json )
            //
            //
            //            }
            //            print(response)
            //            dump(response)
//            dump(NSString(data: data!, encoding:String.Encoding.utf8.rawValue)!)
            if let json = (try? JSONSerialization.jsonObject(with: data!, options: []) as! [Any] ) {
                
                //                var thearray = [[String: Any]]()
                
                DispatchQueue.main.async {
                    //                    print(json)
                    //                    if callback != nil {
                    callback([json])
                    //                    }
                }
                
            }else{
                print("Something went wrong. Couldn't parse json ")
            }
            
        }
        )
        
        task.resume()
        
    }
    
    private static func imagetodata(_ image: UIImage) -> Data? {
        let data = UIImagePNGRepresentation(image)! as Data
        return data
    }
    private static func videotodata(_ video: URL) -> Data? {
        var data = Data()
        do {
            data = try Data(contentsOf: video, options: .mappedIfSafe)
        } catch {
            print(error)
            return nil
        }
        return data
    }
    
    private static func createRequest (post_image: UIImage? = nil, post_video: URL? = nil, urlstring: String, parameters: [[String: String]]? = []) -> NSURLRequest? {
        
        let boundary = generateBoundaryString()
        
        let url = URL( string: urlstring )
        let request = NSMutableURLRequest(url: url! )
        request.httpMethod = "POST"
        
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        return request
    }
    
    private static func createBodyWithParameters(parameters: [[String: Any]]? = nil, boundary: String) -> NSData {
        let body = NSMutableData()
        if parameters != nil {
            for dict in parameters! {
                var filename = ""
                var type = ""
                var mimetype = ""
                var name = ""
                var thevalue: Any = ""
                var valuedata = NSData()
                
                name = dict["name"] as! String
                
                type = dict["type"] as? String ?? ""
                if type == "image" {
                    mimetype = "image/jpg"
                    var data = NSData()
                    if dict["value"] is UIImage {
//                        data = (UIImageJPEGRepresentation(dict["value"] as! UIImage, 1.0) as NSData?)!
                        data = (UIImagePNGRepresentation(dict["value"] as! UIImage) as NSData?)!
                    } else {
                        data = dict["value"] as! NSData
                    }
                    valuedata = data
                    filename = name + ".jpg"
                } else if type == "video" {
                    mimetype = "video/mp4"
                    valuedata = dict["value"] as! NSData
                    filename = name + ".mp4"
                } else if type == "number" {
                    thevalue = dict["value"] ?? 0
                } else if type == "dict" || type == "array" {
                    valuedata = dict["value"]  as! NSData
                } else {
                    thevalue = dict["value"] ?? ""
                }
                
                body.appendString(string:"--\(boundary)\r\n")
                if type == "image" {
                    body.appendString(string: "Content-Disposition: form-data; name=\"\(name)\"; filename=\"\(filename)\"\r\n")
                    body.appendString(string: "Content-Type: \(mimetype)\r\n\r\n")
                    body.append(valuedata as Data)
                    body.appendString(string: "\r\n")
                } else if type == "video" {
                    body.appendString(string: "Content-Disposition: form-data; name=\"\(name)\"; filename=\"\(filename)\"\r\n")
                    body.appendString(string: "Content-Type: \(mimetype)\r\n\r\n")
                    body.append(valuedata as Data)
                    body.appendString(string: "\r\n")
                } else if type == "array" || type == "dict" {
                    body.appendString(string: "Content-Disposition: form-data; name=\"\(name)\"\r\n\r\n")
                    //                    body.appendString(string: "\(thevalue ?? "")\r\n")
                    body.append(valuedata as Data)
                    body.appendString(string: "\r\n")
                } else {
                    body.appendString(string: "Content-Disposition: form-data; name=\"\(name)\"\r\n\r\n")
                    body.appendString(string: "\(thevalue)\r\n")
                }
                body.appendString(string:"--\(boundary)\r\n")
            }
        }
        
        
        return body
    }
    
    private static func mimeTypeForPath(path: String) -> String {
        let url = NSURL(fileURLWithPath: path)
        _ = url.pathExtension
        
        let mimetype = "image/jpg"
//        let mimetype = "image/png"
        return mimetype
        //        if let uti = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, pathExtension! as NSString, nil)?.takeRetainedValue() {
        //            if let mimetype = UTTypeCopyPreferredTagWithClass(uti, kUTTagClassMIMEType)?.takeRetainedValue() {
        //                return mimetype as String
        //            }
        //        }
        //        return "application/octet-stream";
    }
    
    private static func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
    
}
