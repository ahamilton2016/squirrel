//
//  VBBrand.swift
//  MilkTheMoment
//
//  Created by Alexander Hamilton on 1/13/18.
//  Copyright © 2018 Milk The Moment. All rights reserved.
//

import UIKit

class VBBrand: NSObject {
    
    public static var colors = [String: UIColor]()
    
    public struct get {
        public static func color(_ identifier: String ) -> UIColor? {
            return colors[identifier]
        }
    }
    
    public struct set {
        public static func color( color: UIColor, identifier: String ) {
            colors[identifier] = color
        }
    }

}
