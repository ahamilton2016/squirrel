//
//  VBShortcuts.swift
//  MilkTheMoment
//
//  Created by Alexander Hamilton on 1/27/18.
//  Copyright © 2018 Milk The Moment. All rights reserved.
//

import UIKit

class VBShortcuts: NSObject {

    public static func alert( title: String, message: String?="" ) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        //        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: { (action: UIAlertAction) in
            VBcurrentvc.dismiss(animated: true, completion: {
                
            })
        }))
        VBcurrentvc.present(alert, animated: true )
    }
    
}
