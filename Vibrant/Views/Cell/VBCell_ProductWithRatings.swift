//
//  VBCell_ProductWithRatings.swift
//  MilkTheMoment
//
//  Created by Alexander Hamilton on 1/21/18.
//  Copyright © 2018 Milk The Moment. All rights reserved.
//

import UIKit
import ImageLoader

class VBCell_ProductWithRatings: VBCell {

    var celldict: [String: Any]? = [:]
    var viewoptions: [String: Any]? = [:]
    
    var titlelabel = UILabel()
    var descriptionlabel = UILabel()
    var iv = UIImageView()
    
    var separator = UIView()
    
    var ratingsview = UIView()
    var graystarsarray = [UIImageView]()
    var goldstarsarray = [UIImageView]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.viewsetup()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    func viewsetup() {
        VBCell_ProductWithRatings.styling(view: self)
        
        
        
        self.addSubview(titlelabel)
        self.addSubview(iv)
        self.addSubview(ratingsview)
        self.addSubview(descriptionlabel)
        self.addSubview(separator)
    }
    
    
    override func layoutSubviews() {
        
        let title = VBData.getValue(source: celldict!, key: "title") as! String
        let description = VBData.getValue(source: celldict!, key: "description") as! String
        let ratingstring = VBData.getValue(source: celldict!, key: "rating") as? String ?? "0"
        let rating = Int(ratingstring) ?? 0
        let imagepath = VBData.getValue(source: celldict!, key: "imagepath") as? String ?? ""
        
        titlelabel.text = title
        descriptionlabel.text = description
        
        if rating != 0 {
            for i in 0 ... (rating-1) {
                let star = goldstarsarray[i]
                star.isHidden = false
            }
        }
        self.iv.alpha = 0
        if imagepath != "" {
            ImageLoader.request(with: URL(string: imagepath)!, onCompletion: { (image: UIImage?, error: Error?, fetchOperation: FetchOperation) in
                DispatchQueue.main.async {
                    self.iv.image = image!
                    UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: {
                        self.iv.alpha = 1
                    }, completion: { (finished: Bool) in
                        
                    })
                }
            })
        }
        
        separator.frame = CGRect( x: titlelabel.frame.origin.x, y: frame.size.height-0.5, width: self.frame.size.width - (titlelabel.frame.origin.x*2), height: 0.5 )
        
    }
    
    override func prepareForReuse() {
        
        for i in 0 ... 4 {
            let star = goldstarsarray[i]
            star.isHidden = true
        }
        iv.image = UIImage(named: "")
        
    }
    
}
