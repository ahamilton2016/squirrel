//
//  VBCell_Basic.swift
//  MilkTheMoment
//
//  Created by Alexander Hamilton on 1/15/18.
//  Copyright © 2018 Milk The Moment. All rights reserved.
//

import UIKit

class VBCell_Basic: VBCell {

    var celldict: [String: Any]? = [:]
    var viewoptions: [String: Any]? = [:]
    
    var titlelabel = UILabel()
    var descriptionlabel = UILabel()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.viewsetup()
        
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func viewsetup() {
        VBCell_Basic.styling(view: self)
        
        self.addSubview(titlelabel)
        self.addSubview(descriptionlabel)
    }
    
    
    override func layoutSubviews() {
        
        let title = VBData.getValue(source: celldict!, key: "title") as! String
        let description = VBData.getValue(source: celldict!, key: "description") as! String
        
        titlelabel.text = title
        descriptionlabel.text = description
        
    }
    
}
