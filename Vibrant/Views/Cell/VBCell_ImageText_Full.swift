//
//  VBCell_ImageText_Full.swift
//  MilkTheMoment
//
//  Created by Alexander Hamilton on 1/14/18.
//  Copyright © 2018 Milk The Moment. All rights reserved.
//

import UIKit
import ImageLoader

class VBCell_ImageText_Full: VBCell {
    
    var celldict: [String: Any]? = [:]
    var viewoptions: [String: Any]? = [:]
    
    var containerview = UIView()
    var featuredimage = UIImageView()
    var titlelabel = UILabel()
    var descriptionlabel = UILabel()
    var actionbutton = UIButton()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.viewsetup()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func viewsetup() {
        VBCell_ImageText_Full.styling(view: self)
        
        self.addSubview(containerview)
        containerview.addSubview(featuredimage)
        containerview.addSubview(titlelabel)
        containerview.addSubview(descriptionlabel)
        containerview.addSubview(actionbutton)
    }
    
    
    override func layoutSubviews() {
        
        let title = VBData.getValue(source: celldict!, key: "title") as! String
        titlelabel.text = title
        var description = VBData.getValue(source: celldict!, key: "description") as? String ?? ""
        description = description.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
//        description = description.replacingOccurrences(of: "&nbsp;", with: " ", options: .literal, range: nil)
//        description = description.replacingOccurrences(of: "&#39;", with: " ", options: .literal, range: nil)
        let ishtml = viewoptions!["ishtml"] as? Bool ?? false
        if ishtml {
            description = "<p style='float: left; display: inline-block; position: relative; margin: 0; padding: 0; width: 250px; height: 20px; font-size: 16px; text-overflow: ellipsis;'>"+description+"</p><p style='display: inline-block; position: relative; margin: 0; padding: 0; float: left; width: 20px; height: 20px;'>...</p>"
            let attrStr = try! NSAttributedString(
                data: description.data(using: String.Encoding.unicode, allowLossyConversion: true)!,
                options: [
                    .documentType: NSAttributedString.DocumentType.html,
                    .characterEncoding: String.Encoding.utf8.rawValue
                ],
                documentAttributes: nil)
            //        descriptionlabel.font = UIFont.systemFont(ofSize: 20)
            descriptionlabel.attributedText = attrStr
        } else {
            descriptionlabel.text = description
        }
        
        //        descriptionlabel.font = UIFont.systemFont(ofSize: 20)
        let urlstring = VBData.getValue(source: celldict!, key: "imagepath") as! String
        self.featuredimage.alpha = 0
        if urlstring != "" {
            ImageLoader.request(with: URL(string: urlstring)!) { (image: UIImage?, error: Error?, fetch: FetchOperation) in
                DispatchQueue.main.async {
                    self.featuredimage.image = image!
                    UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: {
                        self.featuredimage.alpha = 1
                    }, completion: { (finished: Bool) in
                        
                    })
                }
            }
        }
        
    }
    
    override func prepareForReuse() {
        self.featuredimage.image = UIImage(named: "")
    }
    
}
