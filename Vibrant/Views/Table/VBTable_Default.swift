//
//  VBTable_Default.swift
//  MilkTheMoment
//
//  Created by Alexander Hamilton on 1/14/18.
//  Copyright © 2018 Milk The Moment. All rights reserved.
//

import UIKit

class VBTable_Default: VBTable {
    
    var titlelabel = UILabel()
    
    var cellidentifier = "VBCell_ImageText_Full"
    var tablearray = [[String: Any]]()
    let thistable = UITableView()
    
    var callback: ( UITableView, IndexPath) -> Void = VBTable_Default.tablecallback
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    public func viewsetup( superview: UIView ) {
        VBTable_Default.styling(view: self)
        
        tablearray = viewdict
        if viewoptions["cellname"] != nil {
            cellidentifier = viewoptions["cellname"]! as! String
        }
        
        superview.addSubview(self)
        thistable.estimatedRowHeight = 200.0
        thistable.frame = CGRect(x: 0, y: 0, width: VBviewwidth, height: Double(self.frame.size.height))
        thistable.dataSource = self
        thistable.delegate = self
        thistable.backgroundColor = .init(red: 0, green: 0, blue: 0, alpha: 0)
        thistable.separatorStyle = .none
        self.addSubview(thistable)
        
        VBTable_Default.registernibs(cells: ["VBCell", "VBCell_ImageText_Full", "VBCell_Basic", "VBCell_ProductWithRatings"], table: thistable)
        
        thistable.reloadData()
        //        originaloffsety = thistable.contentOffset.y
        
    }
    
    override func layoutSubviews() {
        
        
        
    }
    
}

// MARK: - UITableViewDelegate
extension VBTable_Default: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        let dict = tablearray[indexPath.row]
        return CGFloat(VBCell.getCellHeight( identifier: cellidentifier ))
    }
    
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//
//        if( loaded ) {
//            if( originaloffsety >= rtable.contentOffset.y-100 ) {
//                if( !attop ) {
//                    attop = true
//                    animateclass.toblur( view: homepageiv, show: false, duration: 0.3 )
//                    animateclass.tocolor(show: false, color: .white, superview: homepageiv, duration: 0.3)
//                }
//            }else{
//                if( attop ) {
//                    attop = false
//                    animateclass.toblur( view: homepageiv, show: true, duration: 0.5 )
//                    animateclass.tocolor(show: true, color: .white, superview: homepageiv, duration: 0.5)
//                    //                    animateclass.towhite( show: true, duration: 0.5 )
//
//                }
//            }
//        }
//    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return tablearray.count
    }
    
}



// MARK: - UITableViewDataSource
extension VBTable_Default: UITableViewDataSource {
    
    class func registernibs( cells: [String], table: UITableView ) {
        for c in cells {
            let nib = UINib(nibName: c, bundle: nil)
            table.register(nib, forCellReuseIdentifier: c)
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
//        var cell = VBCell()
        var cell = VBCell()
        var dict = tablearray[indexPath.row]
        dict = VBData.convertKeys(dict: dict, identifier: identifier)
        if cellidentifier == "VBCell_ImageText_Full" {
            let tempcell = tableView.dequeueReusableCell(withIdentifier: "VBCell_ImageText_Full", for: indexPath) as! VBCell_ImageText_Full
            tempcell.celldict = dict
            tempcell.viewoptions = viewoptions
            cell = tempcell
        } else if cellidentifier == "VBCell_Basic" {
            let tempcell = tableView.dequeueReusableCell(withIdentifier: "VBCell_Basic", for: indexPath) as! VBCell_Basic
            tempcell.celldict = dict
            tempcell.viewoptions = viewoptions
            cell = tempcell
        } else if cellidentifier == "VBCell_ProductWithRatings" {
            let tempcell = tableView.dequeueReusableCell(withIdentifier: "VBCell_ProductWithRatings", for: indexPath) as! VBCell_ProductWithRatings
            tempcell.celldict = dict
            tempcell.viewoptions = viewoptions
            cell = tempcell
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        callback(tableView, indexPath)
        
//        let dict = tablearray[indexPath.row]
//        let type = dict["type"] as? String ?? ""
//        if( type == "event" ) {
//            eventdict = dict
//        }else if( type == "award" || type == "reward" || type == "prize" ) {
//            rewarddict = dict
//        }
//        appactions.cellaction(celldict: dict, indexpath: indexPath, table: tableView)
    }
    
    public static func tablecallback(_ tableView: UITableView, indexPath: IndexPath ) {
        
    }
    
    func reloadWithDataAndOptions( data: [[String: Any]], options: [String: Any]? = nil ) {
        viewdict = data
        if options != nil {
            viewoptions = options!
        }
        tablearray = viewdict
        
        thistable.reloadData()
    }
    
    
}
