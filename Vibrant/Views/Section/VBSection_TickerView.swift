//
//  VBSection_TickerView.swift
//  MilkTheMoment
//
//  Created by Alexander Hamilton on 1/21/18.
//  Copyright © 2018 Milk The Moment. All rights reserved.
//

import UIKit

class VBSection_TickerView: VBSection {

    var viewdict = [String: Any]()
    var viewoptions = [String: Any]()
    var identifier = String()
    
    var titlelabel = UILabel()
    var containerview = UIView()
    var tickerarray = [[String: String]]()
    var tickerviews = [[String: UIView]]()
    
    func viewsetup( superview: UIView ) {
        VBSection_TickerView.styling(view: self)
        
        setvalues()
        
        superview.addSubview(self)
        
        self.addSubview(titlelabel)
        self.addSubview(containerview)
        
    }
    
    func setvalues() {
        
        viewdict = VBData.convertKeys(dict: viewdict, identifier: identifier)
        
        titlelabel.text = VBData.getValue(source: viewdict, key: "title") as? String
        tickerarray = (VBData.getValue(source: viewdict, key: "tickerarray") as? [[String: String]])!
        
        for t in tickerarray {
            let tickerview = UIView()
            let tickertitlelabel = UILabel()
            let tickervaluelabel = UILabel()
            containerview.addSubview(tickerview)
            tickerview.addSubview(tickertitlelabel)
            tickerview.addSubview(tickervaluelabel)
            tickertitlelabel.text = t["title"] ?? ""
            tickervaluelabel.text = t["value"] ?? ""
            let dict = [
                "parentview": tickerview,
                "titlelabel": tickertitlelabel,
                "valuelabel": tickervaluelabel
            ]
            tickerviews.append(dict)
        }
        
        for i in 0...(self.tickerarray.count-1) {
            let tickerview = self.tickerviews[i]["parentview"]!
            let tickertitlelabel = self.tickerviews[i]["titlelabel"] as! UILabel
            let tickervaluelabel = self.tickerviews[i]["valuelabel"] as! UILabel
            
            var originy = CGFloat(30)
            if i > 0 {
                let lastview = self.tickerviews[i-1]["parentview"]!
                originy = lastview.frame.origin.y+lastview.frame.size.height+10
            }
            tickerview.frame = CGRect( x: 20, y: originy, width: CGFloat(containerview.frame.size.width)-40, height: 50 )
            tickertitlelabel.textColor = .white
            tickervaluelabel.textColor = .white
            tickertitlelabel.textAlignment = .left
            tickervaluelabel.textAlignment = .right
            tickertitlelabel.font = UIFont.systemFont(ofSize: 17, weight: UIFont.Weight.bold)
            tickervaluelabel.font = UIFont.systemFont(ofSize: 38, weight: UIFont.Weight.black)
            tickertitlelabel.frame = CGRect( x: 0, y: 0, width: 200, height: 20 )
            tickervaluelabel.frame = CGRect( x: tickerview.frame.size.width-150-0, y: 0, width: 150, height: 40 )
            tickervaluelabel.center = CGPoint( x: tickervaluelabel.center.x, y: tickerview.frame.size.height/2.0 )
            tickertitlelabel.center = CGPoint( x: tickertitlelabel.center.x, y: (tickervaluelabel.center.y+(tickervaluelabel.frame.size.height/2.0))-(tickertitlelabel.frame.size.height/2.0)-5 )
        }
        
        if tickerarray.count > 0 {
            let lastview = tickerviews[tickerviews.count-1]["parentview"]
            self.frame = CGRect( x: self.frame.origin.x, y: self.frame.origin.y, width: self.frame.size.width, height: (lastview?.frame.origin.y)!+(lastview?.frame.size.height)!+50 )
            containerview.frame = CGRect( x: containerview.frame.origin.x, y: containerview.frame.origin.y, width: containerview.frame.size.width, height: (lastview?.frame.origin.y)!+(lastview?.frame.size.height)!+40 )
        }
        
    }
    
    override func layoutSubviews() {
        
        
        
    }
    
    func refreshdata() {
        var i=0
        tickerarray = (VBData.getValue(source: viewdict, key: "tickerarray") as? [[String: String]])!
        for t in tickerarray {
            let tickerview = tickerviews[i]["parentview"]
            let tickertitlelabel = tickerviews[i]["titlelabel"] as! UILabel
            let tickervaluelabel = tickerviews[i]["valuelabel"] as! UILabel
            tickertitlelabel.text = t["title"] ?? ""
            tickervaluelabel.text = t["value"] ?? ""
            i+=1
        }
    }

}
