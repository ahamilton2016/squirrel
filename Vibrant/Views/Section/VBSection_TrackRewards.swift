//
//  VBSection_TrackRewards.swift
//  MilkTheMoment
//
//  Created by Alexander Hamilton on 1/14/18.
//  Copyright © 2018 Milk The Moment. All rights reserved.
//

import UIKit

class VBSection_TrackRewards: VBSection {
    
    var viewdict = [[String: Any]]()
    var viewoptions = [String: Any]()
    var identifier = String()
    
    var titlelabel = UILabel()
    var pointvalue = UILabel()
    var pointlabel = UILabel()
    var messagelabel = UILabel()
    var barview = UIView()
    var fillview = UIView()
    
    func viewsetup( superview: UIView ) {
        VBSection_TrackRewards.styling(view: self)
        
        superview.addSubview(self)
        
        self.addSubview(titlelabel)
        self.addSubview(pointvalue)
        self.addSubview(pointlabel)
        self.addSubview(messagelabel)
        self.addSubview(barview)
        barview.addSubview(fillview)
    }
    
    func setTitle(_ text: String ) {
        titlelabel.text = text
    }
    
    func setPointValue(_ text: String ) {
        pointvalue.text = text
    }
    
    func setPointLabel(_ text: String ) {
        pointlabel.text = text
    }
    
    func setMessage(_ text: String ) {
        messagelabel.text = text
    }
    
    func setBar( percentage: Double ) {
        let fillwidth = Double(barview.frame.size.width) * percentage
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: {
            self.fillview.frame = CGRect( origin: self.fillview.frame.origin, size: CGSize( width: fillwidth-Double(self.fillview.frame.origin.x*2), height: Double(self.fillview.frame.size.height) ) )
        }) { (finished: Bool) in
            
        }
    }
    
    func setFillColor(_ color: UIColor ) {
        fillview.backgroundColor = color
    }
    
    func setTextColor(_ color: UIColor ) {
        titlelabel.textColor = color
        pointvalue.textColor = color
        pointlabel.textColor = color
        messagelabel.textColor = color
    }
    
    override func layoutSubviews() {
        
        
        
    }
    
}
