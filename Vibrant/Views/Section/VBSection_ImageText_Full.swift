//
//  VBSection_ImageText_Full.swift
//  MilkTheMoment
//
//  Created by Alexander Hamilton on 1/17/18.
//  Copyright © 2018 Milk The Moment. All rights reserved.
//

import UIKit
import ImageLoader


class VBSection_ImageText_Full: VBSection {
    
    var viewdict = [String: Any]()
    var viewoptions = [String: Any]()
    var identifier = String()
    
    var containerview = UIView()
    var featuredimage = UIImageView()
    var titlelabel = UILabel()
    var descriptionlabel = UILabel()
    var actionbutton = UIButton()
    
    func viewsetup( superview: UIView ) {
        VBSection_ImageText_Full.styling(view: self)
        
        setvalues()
        
        superview.addSubview(self)
        
        self.addSubview(containerview)
        containerview.addSubview(featuredimage)
        containerview.addSubview(titlelabel)
        containerview.addSubview(descriptionlabel)
        containerview.addSubview(actionbutton)
    }
    
    func setvalues() {
        
        viewdict = VBData.convertKeys(dict: viewdict, identifier: identifier)
        
        titlelabel.text = VBData.getValue(source: viewdict, key: "title") as? String
        var description = VBData.getValue(source: viewdict, key: "description") as? String
        description = "<style>body{font-size:16px;}</style>"+description!
        let attrStr = try! NSAttributedString(
            data: description!.data(using: String.Encoding.unicode, allowLossyConversion: true)!,
            options: [
                .documentType: NSAttributedString.DocumentType.html,
                .characterEncoding: String.Encoding.utf8.rawValue
            ],
            documentAttributes: nil)
//        descriptionlabel.font = UIFont.systemFont(ofSize: 20)
        descriptionlabel.attributedText = attrStr
        descriptionlabel.numberOfLines = 0
//        descriptionlabel.text = description
        
        let descriptionsize = descriptionlabel.sizeThatFits( CGSize( width: descriptionlabel.frame.size.width, height: CGFloat(MAXFLOAT) ) )
        descriptionlabel.frame = CGRect( x: descriptionlabel.frame.origin.x, y: descriptionlabel.frame.origin.y, width: descriptionlabel.frame.size.width, height: descriptionsize.height )
        let urlstring = VBData.getValue(source: viewdict, key: "imagepath") as! String
        if urlstring != "" {
            ImageLoader.request(with: URL(string: urlstring)!) { (image: UIImage?, error: Error?, fetch: FetchOperation) in
                DispatchQueue.main.async {
                    self.featuredimage.image = image!
                }
            }
        }
        
        containerview.frame = CGRect( x: containerview.frame.origin.x, y: containerview.frame.origin.y, width: containerview.frame.size.width, height: descriptionlabel.frame.origin.y+descriptionlabel.frame.size.height+10 )
        self.frame = CGRect( x: self.frame.origin.x, y: self.frame.origin.y, width: self.frame.size.width, height: containerview.frame.origin.y+containerview.frame.size.height+10 )
        
    }
    
    override func layoutSubviews() {
        
        
        
    }
    
    
    
    
}
