//
//  VBTextfield.swift
//  MilkTheMoment
//
//  Created by Alexander Hamilton on 1/12/18.
//  Copyright © 2018 Milk The Moment. All rights reserved.
//

import UIKit

class VBTextfield: VBInput, UITextFieldDelegate {
    
    var viewdict = [String: Any]()
    var viewoptions = [String: Any]()
    var identifier = String()
    
    var tf = UITextField()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    func viewsetup( superview: UIView ) {
        VBTextfield.styling(view: self)
        
        let placeholder = VBData.getValue(source: viewoptions, key: "placeholder") as? String
        tf.placeholder = placeholder
        tf.delegate = self
        superview.addSubview(self)
        self.addSubview(tf)
    }
    
    override func layoutSubviews() {
        
        
        
    }


}

extension VBTextfield {
    
    func placeholder(_ text: String ) {
        self.tf.attributedPlaceholder = NSAttributedString(string: text, attributes: [NSAttributedStringKey.foregroundColor: UIColor.init(red: (153/255.0), green: (153/255.0), blue: (153/255.0), alpha: 1.0)])
    }
    
}

// delegate functions
extension VBTextfield {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(tapped))
        VBcurrentvc.view.addGestureRecognizer(tap)
        
    }
    
    @objc func tapped( tap: UITapGestureRecognizer ) {
        VBcurrentvc.view.removeGestureRecognizer(tap)
        tf.resignFirstResponder()
    }
}

