//
//  VBInput_Picker.swift
//  Squirrel
//
//  Created by Alexander Hamilton on 1/28/18.
//  Copyright © 2018 Vibrant, LLC. All rights reserved.
//

import UIKit

class VBInput_Picker: VBInput, UIPickerViewDelegate, UIPickerViewDataSource {

    var viewdict = [String: Any]()
    var viewoptions = [String: Any]()
    var identifier = String()
    
    let pickerview = UIPickerView()
    var pickerarray = [[String: Any]]()
    
    var button = UIButton()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    func viewsetup( superview: UIView ) {
        
        pickerarray = VBData.getValue(source: viewdict, key: "pickerarray") as? [[String: Any]] ?? [[:]]
        
        VBInput_Picker.styling(view: self)
        
        button.addTarget(self, action: #selector(buttonaction(sender:)), for: .touchUpInside)
        
        let placeholder = VBData.getValue(source: viewoptions, key: "placeholder") as? String
        superview.addSubview(self)
        self.addSubview(button)
        
        pickerview.frame = CGRect( x: 0, y: VBviewheight-200, width: VBviewwidth, height: 200 )
        pickerview.delegate = self
        pickerview.dataSource = self
        pickerview.backgroundColor = .white
        VBcurrentvc.view.addSubview(pickerview)
        pickerview.reloadAllComponents()
        pickerview.alpha = 0
        
        let row = pickerview.selectedRow(inComponent: 0)
        pickerView(pickerview, didSelectRow: row, inComponent:0)
        
    }
    
    override func layoutSubviews() {
        
        
        
    }
    
    @objc func buttonaction( sender: UIButton ) {
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(tapped))
        VBcurrentvc.view.addGestureRecognizer(tap)
        showpicker(true)
    }
    
    @objc func tapped( tap: UITapGestureRecognizer ) {
        VBcurrentvc.view.removeGestureRecognizer(tap)
        showpicker(false)
    }
    
    func showpicker(_ show: Bool=true) {
        if show {
            UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: {
                self.pickerview.alpha = 1
            }) { (finished: Bool) in
                
            }
        } else {
            UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: {
                self.pickerview.alpha = 0
            }) { (finished: Bool) in
                
            }
        }
    }
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerarray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerarray[row]["title"] as? String ?? ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let title = pickerarray[row]["title"] as? String ?? ""
        button.setTitle(title, for: .normal)
    }

}
