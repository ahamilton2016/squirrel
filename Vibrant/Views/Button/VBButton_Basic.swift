//
//  VBButton_Basic.swift
//  MilkTheMoment
//
//  Created by Alexander Hamilton on 1/13/18.
//  Copyright © 2018 Milk The Moment. All rights reserved.
//

import UIKit

class VBButton_Basic: VBButton {

    var button = UIButton()
    
    func viewsetup( superview: UIView ) {
        VBButton_Basic.styling(view: self)
        
        superview.addSubview(self)
        self.addSubview(button)
    }
    
    override func layoutSubviews() {
        
        
        
    }

}
