//
//  SummationViewController.swift
//  Squirrel
//
//  Created by Alexander Hamilton on 2/8/18.
//  Copyright © 2018 Vibrant, LLC. All rights reserved.
//

import UIKit

class SummationViewController: UIViewController {

    var tablearray = [[String: Any]]()
    override func viewWillAppear(_ animated: Bool) {
        defaultVCSettings()
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        defaultVCSettings()
        
        VBPage.start()
        
        setuparrays()
        VBTable.set(superview: self.view, filename: "VBTable_Default", identifier: "main_table")
        
        VBPage.end()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func setuparrays() {
        tablearray = [
            ["title": "Experience Nashville"],
            ["title": "Poplar Hill Realty"],
            ["title": "Evans Company"],
            ["title": "Digi"],
            ["title": "Fork + Spoon"],
            ["title": "Vibrant, LLC"],
        ]
        
        VBData.addData(data: tablearray, identifier: "main_table")
        VBData.addOption(key: "cellname", value: "VBCell_Basic", identifier: "main_table")
        
    }

    

}
