//
//  HomeViewController.swift
//  Squirrel
//
//  Created by Alexander Hamilton on 1/28/18.
//  Copyright © 2018 Vibrant, LLC. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func defaultVCSettings() {
        setVC(self)
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.barTintColor = VBBrand.get.color("primary")
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
        self.navigationController?.navigationBar.tintColor = UIColor.white;
    }
}

class HomeViewController: UIViewController {
    
    var sv = UIScrollView()
    
    var company_dropdown = VBInput_Picker()
    var businessname_tf = VBTextfield()
    var contactfullname_tf = VBTextfield()
    var email_tf = VBTextfield()
    var phonenumber_tf = VBTextfield()
    
    var savebutton = VBButton_Basic()

    override func viewWillAppear(_ animated: Bool) {
        defaultVCSettings()
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
    }
    
    func setData() {
        // company_dropdown
        let companyarray = [
            ["title": "CHOOSE COMPANY..."],
            ["title": "Evans Company"],
            ["title": "Digi"],
            ["title": "Fork + Spoon"],
            ["title": "Experience Nashville"],
            ["title": "EntreNash"],
        ]
        let companydict = ["pickerarray": companyarray]
        VBData.addData(data: companydict, identifier: "company_dropdown")
        VBData.addOption(key: "placeholder", value: "SELECT COMPANY...", identifier: "company_dropdown")
        
        // businessname_tf
        VBData.addOption(key: "placeholder", value: "BUSINESS NAME", identifier: "businessname_tf")
        
        // contactfullname_tf
        VBData.addOption(key: "placeholder", value: "CONTACT FULL NAME", identifier: "contactfullname_tf")
        
        // email_tf
        VBData.addOption(key: "placeholder", value: "EMAIL", identifier: "email_tf")
        
        // phonenumber_tf
        VBData.addOption(key: "placeholder", value: "PHONE NUMBER", identifier: "phonenumber_tf")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        defaultVCSettings()
        
        
        let summationbutton = UIButton(type: UIButtonType.custom)
        summationbutton.setImage(UIImage(named: "users_icon@3x.png"), for: .normal)
        summationbutton.addTarget(self, action: #selector(summationbuttonaction(sender:)), for: .touchUpInside)
        summationbutton.frame = CGRect( x: 0, y: 0, width: 30, height: 30)
        summationbutton.clipsToBounds = true
        summationbutton.layer.cornerRadius = 15
        summationbutton.layer.masksToBounds = true
        summationbutton.imageView?.contentMode = .scaleAspectFill
        let barButton = UIBarButtonItem(customView: summationbutton)
        self.navigationItem.rightBarButtonItem = barButton
        
        self.view.backgroundColor = .white
        
        sv.frame = self.view.frame
        self.view.addSubview(sv)
        sv.contentSize = CGSize( width: sv.frame.size.width, height: self.view.frame.size.height*2 )
        
        let logoview = UIImageView()
        logoview.frame = CGRect( origin: .zero, size: CGSize( width: 150, height: 50 ) )
        logoview.contentMode = .scaleAspectFit
        logoview.image = UIImage( named: "digilogo_small.png" )
        sv.addSubview(logoview)
        
        logoview.center = CGPoint( x: VBviewwidth/2.0, y: Double(logoview.frame.size.height/2.0)+40 )
        
        VBPage.start()
        
        setData()
        
        VBInput.set(superview: sv, filename: "VBInput_Picker", identifier: "company_dropdown")
        VBInput.set(superview: sv, filename: "VBTextfield", identifier: "businessname_tf")
        VBInput.set(superview: sv, filename: "VBTextfield", identifier: "contactfullname_tf")
        VBInput.set(superview: sv, filename: "VBTextfield", identifier: "email_tf")
        VBInput.set(superview: sv, filename: "VBTextfield", identifier: "phonenumber_tf")
        VBButton.set(superview: sv, filename: "VBButton_Basic", identifier: "save_button")
        
        VBPage.end()
        
        company_dropdown = VBCollection.getView(identifier: "company_dropdown") as! VBInput_Picker
        businessname_tf = VBCollection.getView(identifier: "businessname_tf") as! VBTextfield
        contactfullname_tf = VBCollection.getView(identifier: "contactfullname_tf") as! VBTextfield
        email_tf = VBCollection.getView(identifier: "email_tf") as! VBTextfield
        phonenumber_tf = VBCollection.getView(identifier: "phonenumber_tf") as! VBTextfield
        savebutton = VBCollection.getView(identifier: "save_button") as! VBButton_Basic
        
        VBStyling.placeUnderView(view: company_dropdown, underview: logoview)
        VBStyling.addMarginTop(company_dropdown, margin: 20)
        VBStyling.repositionViews()
        
        savebutton.button.backgroundColor = UIColor.init(red: (218/255.0), green: (66/255.0), blue: (92/255.0), alpha: 1)
        savebutton.button.setTitleColor(.white, for: .normal)
        savebutton.button.setTitle("SUBMIT", for: .normal)
        savebutton.button.addTarget(self, action: #selector(savebuttonaction(sender:)), for: .touchUpInside)
        
        let buttonsize = CGFloat(80)
        
        let firstbutton = UIButton()
        firstbutton.frame = CGRect( x: 15, y: savebutton.frame.origin.y+savebutton.frame.size.height+20, width: buttonsize, height: buttonsize )
        firstbutton.backgroundColor = .white
        firstbutton.layer.cornerRadius = 10
        firstbutton.layer.masksToBounds = true
        firstbutton.layer.borderColor = UIColor.init(red: (51/255.0), green: (51/255.0), blue: (51/255.0), alpha: 1).cgColor
        firstbutton.layer.borderWidth = 1
        firstbutton.setTitle("1", for: .normal)
        firstbutton.titleLabel?.font = UIFont(name: "AvenirNext-Regular", size: 40)
        firstbutton.setTitleColor(.darkGray, for: .normal)
        firstbutton.addTarget(self, action: #selector(firstbuttonaction(sender:)), for: .touchUpInside)
        sv.addSubview(firstbutton)
        
        let secondbutton = UIButton()
        secondbutton.frame = CGRect( x: firstbutton.frame.origin.x+firstbutton.frame.size.width+10, y: savebutton.frame.origin.y+savebutton.frame.size.height+20, width: buttonsize, height: buttonsize )
        secondbutton.backgroundColor = .white
        secondbutton.layer.cornerRadius = 10
        secondbutton.layer.masksToBounds = true
        secondbutton.layer.borderColor = UIColor.init(red: (51/255.0), green: (51/255.0), blue: (51/255.0), alpha: 1).cgColor
        secondbutton.layer.borderWidth = 1
        secondbutton.setTitle("2", for: .normal)
        secondbutton.titleLabel?.font = UIFont(name: "AvenirNext-Regular", size: 40)
        secondbutton.setTitleColor(.darkGray, for: .normal)
        sv.addSubview(secondbutton)
        
        firstbutton.center = CGPoint( x: (VBviewwidth/2.0) - Double(secondbutton.frame.size.width/2.0) - 20, y: Double(firstbutton.center.y) )
        secondbutton.center = CGPoint( x: (VBviewwidth/2.0) + Double(secondbutton.frame.size.width/2.0) + 20, y: Double(secondbutton.center.y) )
        
        
        company_dropdown.button.backgroundColor = .white
        company_dropdown.button.setTitle("SELECT COMPANY...", for: .normal)
        company_dropdown.button.titleLabel?.font = UIFont(name: "AvenirNext-Regular", size: 16)
        company_dropdown.button.setTitleColor(UIColor.init(red: (51/255.0), green: (51/255.0), blue: (51/255.0), alpha: 1), for: .normal)
        
        businessname_tf.tf.font = UIFont(name: "AvenirNext-Regular", size: 16)
        businessname_tf.tf.textAlignment = .center
        contactfullname_tf.tf.font = UIFont(name: "AvenirNext-Regular", size: 16)
        contactfullname_tf.tf.textAlignment = .center
        email_tf.tf.font = UIFont(name: "AvenirNext-Regular", size: 16)
        email_tf.tf.textAlignment = .center
        phonenumber_tf.tf.font = UIFont(name: "AvenirNext-Regular", size: 16)
        phonenumber_tf.tf.textAlignment = .center
        
        savebutton.button.titleLabel?.font = UIFont(name: "AvenirNext-Bold", size: 16)
        
        let lineView = UIView(frame: CGRect(x: 0, y: savebutton.button.frame.size.height-3, width: savebutton.button.frame.size.width, height: 3))
        lineView.backgroundColor=UIColor.black
        savebutton.button.addSubview(lineView)
        
        UIFont.familyNames.forEach({ familyName in
            let fontNames = UIFont.fontNames(forFamilyName: familyName)
            print(familyName, fontNames)
        })
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func savebuttonaction( sender: UIButton) {
        let email = email_tf.tf.text?.lowercased() ?? ""
//        company_dropdown.
//        let companyname = company_dropdown.tf.text?.lowercased() ?? ""
        let businessname = businessname_tf.tf.text?.lowercased() ?? ""
        let fullname = contactfullname_tf.tf.text?.lowercased() ?? ""
        let phonenumber = phonenumber_tf.tf.text?.lowercased() ?? ""
        let params = [
            ["name": "email", "value": email, "type": "string"],
//            ["name": "company_name", "value": email, "type": "string"],
            ["name": "business_name", "value": businessname, "type": "string"],
            ["name": "full_name", "value": fullname, "type": "string"],
            ["name": "phone_number", "value": phonenumber, "type": "string"],
        ] as [[String: Any]]
        VBRequest.postRequest(urlstring: "https://theanywherecard.com/squirrel/api/signup_list.php", params: params, callback: self.subscribedtolist)
        
    }
    
    func subscribedtolist( json: [Any] ) {
        print(json)
        VBShortcuts.alert(title: "Subscribed!")
        businessname_tf.tf.text = ""
        contactfullname_tf.tf.text = ""
        email_tf.tf.text = ""
        phonenumber_tf.tf.text = ""
    }
    
    @objc func firstbuttonaction( sender: UIButton ) {
        let params = [] as [[String: Any]]
        VBRequest.postRequest(urlstring: "https://theanywherecard.com/squirrel/api/mailchimp/send_campaign.php", params: params, callback: self.firstbuttonactioncallback)
        VBShortcuts.alert(title: "Email Sent!")
    }
    
    func firstbuttonactioncallback(  json: [Any]  ) {
        print(json)
//        VBShortcuts.alert(title: "Email Sent!")
    }
    
    @objc func summationbuttonaction( sender: UIButton ) {
        VBNavigate.push(vc: "SummationViewController")
    }


}

